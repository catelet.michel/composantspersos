package fr.mc.listSelection;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;

import java.util.List;




public class ListSelectionBean<T> {

    private  ObservableList<T> disponible;
    private ObservableList<T> selected;
    private FilteredList<T>disponibleFiltre;
    private FilteredList<T>selectedFiltre;


    public ListSelectionBean() {
        this.disponible = FXCollections.observableArrayList();
        this.selected = FXCollections.observableArrayList();
        this.disponibleFiltre = new FilteredList<>(disponible);
        this.selectedFiltre = new FilteredList<>(selected);
    }

    public ObservableList<T> getDisponible() {
        return disponible;
    }



    public ObservableList<T> getSelected() {
        return selected;
    }



    public void setDisponibleFiltre(FilteredList<T> disponibleFiltre) {
        this.disponibleFiltre = disponibleFiltre;
    }

    public void setSelectedFiltre(FilteredList<T> selectedFiltre) {
        this.selectedFiltre = selectedFiltre;
    }

    public ObservableList<T> getDisponibleFiltre() {
        return disponibleFiltre;
    }

    public ObservableList<T> getSelectedFiltre() {
        return selectedFiltre;

    }

    public void filtrer(String newValue) {

    }

    public void setLists(List<T> available, List<T> selected) {
        for (T elementLu: available){
            if (!selected.contains(elementLu))
                this.disponible.add(elementLu);

        }
        this.selected.addAll(selected);

    }

    public void selectAll(){
        selected.addAll(disponibleFiltre);
        disponible.removeAll(selectedFiltre);
    }

    public void unSelectAll(){
        selected.removeAll(disponibleFiltre);
        disponible.addAll(selectedFiltre);
    }

    public void unSelect(ObservableList<T> liste){
        disponible.addAll(liste);
        selected.removeAll(liste);
    }
}
