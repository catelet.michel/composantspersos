package fr.mc.listSelection;

import fr.mc.projet5controlledtextfield.ControlledTextField;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TestList extends Application {
    @Override
    public void start(Stage stage) throws IOException {


        ArrayList available = new ArrayList();
        available.add("Rouge");
        available.add("Bleu");
        available.add("Vert");

        ArrayList selected = new ArrayList();
        selected.add("Jaune");



        ListSelection maListeSelection = new ListSelection();
        maListeSelection.setLists(available,selected);





        VBox rootLayout = new VBox();
        rootLayout.getChildren().add(maListeSelection);


        stage.setTitle("listSelection!");
        stage.setScene(new Scene(rootLayout));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}