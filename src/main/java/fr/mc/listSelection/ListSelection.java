package fr.mc.listSelection;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.List;

public class ListSelection<T> extends BorderPane {

    @FXML
    private ListView<T> listDispo;

    @FXML
    private ListView<T> selectedList;

    @FXML
    private TextField textField;
    @FXML
    private Button selectOne;
    @FXML
    private Button selectAll;
    @FXML
    private Button unSelectOne;
    @FXML
    private Button unSelectAll;
    @FXML
    private AnchorPane anchorPaneFiltre;

    private final ListSelectionBean<T>bean;

    private BorderPane  borderPane;

    public ListSelection(){

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("listSelection.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            borderPane = fxmlLoader.load();
        }catch(IOException exception){

            exception.printStackTrace();

        }

        bean=new ListSelectionBean<>();

        loadComposant();
        filtered(false);
        listDispo=new ListView<>();
        listDispo.setItems(bean.getDisponibleFiltre());
        selectedList=new ListView<>();
        selectedList.setItems(bean.getSelectedFiltre());
        setItemClick();
        setDragStart();
        setDragOver();
        setDragDropped();
//        textField.textProperty().addListener((observable, oldValue,newValue) -> bean.filtrer(newValue));
    }

    public void setLists(List<T>available,List<T>selected){
        bean.setLists(available,selected);
    }






    private void setDragStart() {
        listDispo.setOnDragDetected(dragEvent-> {
            final Dragboard dragboard= listDispo.startDragAndDrop(TransferMode.MOVE);
            final ClipboardContent content = new ClipboardContent();
            content.putString("selectionner");
            dragboard.setContent(content);
            dragEvent.consume();
        });
        selectedList.setOnDragDetected(dragEvent-> {
            final Dragboard dragboard= selectedList.startDragAndDrop(TransferMode.MOVE);
            final ClipboardContent content = new ClipboardContent();
            content.putString("selectionner");
            dragboard.setContent(content);
            dragEvent.consume();
        });
    }

    private void setDragOver() {

        listDispo.setOnDragOver(dragEvent -> {
            if(dragEvent.getGestureSource()==selectedList  ){
                dragEvent.acceptTransferModes(TransferMode.MOVE);
                dragEvent.consume();
            }
        });

        selectedList.setOnDragOver(dragEvent -> {
            if(dragEvent.getGestureSource()==listDispo  ){
                dragEvent.acceptTransferModes(TransferMode.MOVE);
                dragEvent.consume();
            }
        });


//        anchorPaneFiltre.setOnDragOver(dragEvent -> {
//            if(dragEvent.getGestureSource()==listDispo && listDispo.getSelectionModel().getSelectedItems().size() == 1 ){
//                dragEvent.acceptTransferModes(TransferMode.MOVE);
//                dragEvent.consume();
//            }
//            if(dragEvent.getGestureSource()==selectedList && selectedList.getSelectionModel().getSelectedItems().size() == 1 ){
//                dragEvent.acceptTransferModes(TransferMode.MOVE);
//                dragEvent.consume();
//            }

//        });



    }



    private void setDragDropped() {
        selectedList.setOnDragDropped(dragEvent -> {
            if(dragEvent.getGestureSource()==listDispo)
                listDispo.getItems();
            dragEvent.getDragboard();

            dragEvent.consume();

        });

        listDispo.setOnDragDropped(dragEvent -> {
            dragEvent.getDragboard();
            dragEvent.consume();

        });

//        anchorPaneFiltre.setOnDragDropped(dragEvent -> {
//            dragEvent.getDragboard();
//            dragEvent.consume();
//
//        });


    }

    private void setItemClick() {
        listDispo.setOnMouseClicked(event -> {
            if(event.getClickCount()==2)
                selectOne();
            if(event.getClickCount()==3)
                selectAll();
        });

        selectedList.setOnMouseClicked(event -> {
            if(event.getClickCount()==2)
                unSelectOne();
            if(event.getClickCount()==3)
                unSelectAll();
        } );
    }

    private void unSelectAll() {
    }

    private void unSelectOne() {
    }

    private void selectAll() {
    }

    private void selectOne() {
    }

    private void filtered(boolean b) {
    }

    private void loadComposant() {
    }
}
