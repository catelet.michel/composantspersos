package fr.mc.projet5controlledtextfield;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

import java.io.IOException;

public class ControlledTextField extends BorderPane {

    @FXML
    private TextField textField;

    @FXML
    private Label labelText;

    @FXML
    private Label labelError;

    private boolean correct;

    private boolean horizontale;

    private BorderPane borderPane;


    public ControlledTextField (){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("controlledTextField.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            borderPane = fxmlLoader.load();
        }catch(IOException exception){

            exception.printStackTrace();

        }
        labelText.setText("");
        labelError.setText("");


    }

    public void setLabelText(String label){

        labelText.setText(label);
    }

    public void setHorizontale(Boolean bool){
        BorderPane.setAlignment(labelText, Pos.CENTER_LEFT);
        BorderPane.setAlignment(labelError, Pos.CENTER_LEFT);
    if(bool){
        borderPane.setTop(null);
        borderPane.setBottom(null);
        borderPane.setLeft(labelText);
        borderPane.setRight(labelError);
    }else{
        borderPane.setTop(labelText);
        borderPane.setBottom(labelError);
        borderPane.setLeft(null);
        borderPane.setRight(null);
    }
    }

    public void setParameters(String textCorrect,String textIncorrect,String expressionReguliere){
        matchExpression(textCorrect,textIncorrect,expressionReguliere,textField.getText());
        textField.textProperty().addListener((obs, oldtext, newtext) -> matchExpression(textCorrect, textIncorrect, expressionReguliere,newtext));
    }

    private void matchExpression(String textCorrect, String textIncorrect, String expressionReguliere, String newtext) {
    if(newtext.matches(expressionReguliere)){
        labelError.setText(textCorrect);
        labelError.setTextFill(Color.GREEN);
        correct=true;
    }else {
        labelError.setText(textIncorrect);
        labelError.setTextFill(Color.RED);
        correct=false;
    }
    }


}








