package fr.mc.projet5controlledtextfield;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class Test extends Application {
    @Override
    public void start(Stage stage) throws IOException {

        ControlledTextField testControlledTextField = new ControlledTextField();
        testControlledTextField.setLabelText("codePostal");
        testControlledTextField.setHorizontale(true);
        testControlledTextField.setParameters("ok","incorrect", "(^0[1-9]|^1[0-9]|^2[1-9]|^[3-9][0-9]|^2A|^2B)[0-9]{3}");
        ControlledTextField testControlledTextMotDePasse = new ControlledTextField();
        testControlledTextMotDePasse.setLabelText("MDP");
        testControlledTextMotDePasse.setHorizontale(true);
        testControlledTextMotDePasse.setParameters("ok","incorrect","^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,15}$");
        ControlledTextField testControlledTextTelephone = new ControlledTextField();
        testControlledTextTelephone.setLabelText("tel");
        testControlledTextTelephone.setHorizontale(true);
        testControlledTextTelephone.setParameters("ok","incorrect","(0|\\+33)[1-9]( *[0-9]{2}){4}");



        VBox rootLayout = new VBox();
        rootLayout.getChildren().add(testControlledTextField);
        rootLayout.getChildren().add(testControlledTextMotDePasse);
        rootLayout.getChildren().add(testControlledTextTelephone);

        stage.setTitle("Test!");
        stage.setScene(new Scene(rootLayout));
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}