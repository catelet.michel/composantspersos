module fr.mc.controlledtextfield.projet5controlledtextfield {
    requires javafx.controls;
    requires javafx.fxml;


    opens fr.mc.projet5controlledtextfield to javafx.fxml;
    exports fr.mc.projet5controlledtextfield;
    exports fr.mc.listSelection;
    opens fr.mc.listSelection to javafx.fxml;
}